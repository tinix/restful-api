FactoryBot.define do
  factory :user do
    sequence(:login) { |n| "dtinviella#{n}" }
    name {"Daniel Tinivella"}
    url {"http://danieltinivella.com"}
    avatar_url {"http://danieltinivella.com/about/"}
    provider {"github"}
  end
end
